Proceso  multaOperadores
	
	Dimension multas[8,4]	
	claroInternet   = 0
	claroTelefonica = 0
	etbInternet   = 0
	etbTelefonica = 0
	movistarInternet   = 0
	movistarTelefonica = 0
	tigoUneInternet   = 0
	tigoUneTelefonica = 0
	
	Claro    = 0
	Movistar = 0
	Etb      = 0
	TigoUne  = 0	
	
	Dimension multasOperador[4]
	
	mayorMulta         = 0
	posicionMayorMulta = 0
	
	// Ingresar datos de las multas en la matriz
	Para fila=1 hasta 8 Hacer
		para col=1 hasta 4 Hacer
			multas[fila, col] = Aleatorio(1000,8000)
			Escribir "La matriz es: multas[", fila, ",", col, "]=", multas[fila, col]
		FinPara
	FinPara	
	
	// total multa por cada servicio
	Para fila = 1 hasta 8 Hacer
		
		para col=1 hasta 4 Hacer
			
			Si fila = 1 Entonces
				claroInternet = claroInternet + multas[fila, col] 
			FinSi
			
			Si fila = 2 Entonces
				claroTelefonica = claroTelefonica + multas[fila, col] 
				Claro = claroInternet + claroTelefonica
				multasOperador[1] = Claro
			FinSi
			
			Si fila = 3 Entonces
				etbInternet = etbInternet + multas[fila, col] 
			FinSi
			
			Si fila = 4 Entonces
				etbTelefonica = etbTelefonica + multas[fila, col] 
				Etb = etbInternet + etbTelefonica
				multasOperador[2] = Etb
			FinSi
			
			Si fila = 5 Entonces
				movistarInternet = movistarInternet + multas[fila, col] 
			FinSi
			
			Si fila = 6 Entonces
				movistarTelefonica = movistarTelefonica + multas[fila, col] 
				Movistar = movistarInternet + movistarTelefonica
				multasOperador[3] = Movistar
			FinSi
			
			Si fila = 7 Entonces
				tigoUneInternet = tigoUneInternet + multas[fila, col] 
			FinSi
			
			Si fila = 8 Entonces
				tigoUneTelefonica = tigoUneTelefonica + multas[fila, col] 
				TigoUne = tigoUneInternet + tigoUneTelefonica
				multasOperador[4] = TigoUne
			FinSi
					
	FinPara
		
	FinPara
	
	Escribir "********** Multas de Cada Operador ********** "
	Escribir "Multas claro ", Claro
	Escribir "Multas Etb ", Etb
	Escribir "Multas Movistar ", Movistar
	Escribir "Multas Tigo Une ", TigoUne
	
	// Operador con mayor multas
	    
	Escribir "********** Operador con mayor multas ********** "	
	
	Para inicio = 1 Hasta 4 Con Paso 1 Hacer
		
		Si multasOperador[inicio] > mayorMulta Entonces
			mayorMulta = multasOperador[inicio]
			posicionMayorMulta = inicio	
		FinSi
		
	Fin Para
	
	Si posicionMayorMulta = 1 Entonces
		Escribir "Operador claro : ", mayorMulta
	FinSi
	Si posicionMayorMulta = 2 Entonces
		Escribir "Operador Etb : ", mayorMulta
	FinSi
	Si posicionMayorMulta = 3 Entonces
		Escribir "Operador Movistar : ", mayorMulta
	FinSi
	Si posicionMayorMulta = 4 Entonces
		Escribir "Operador Tigo Une : ", mayorMulta
	FinSi
	
	
	
	
	
FinProceso

