
Proceso  cuadroMagico
	
	Escribir "Cuadrado magico"
	Escribir "Ingresar el numero de filas"
	Leer filas
	Escribir "Ingresar el numero de columnas"
	Leer columnas
	
	Contador        = 0
	nuevafila       = 0
	nuevaColumna    = 0
	filaAnterior    = 0
	ColumnaAnterior = 0
	
	tamaņoMatriz = filas * columnas
	
	Dimension lista[filas,columnas]
	
	// Iniciamos el primer valor 1
	nuevaColumna    = (filas / 2) + 0.5
	nuevafila       = 1
	filaAnterior    = nuevafila
	ColumnaAnterior = nuevaColumna
	lista[nuevafila,nuevaColumna] = 1	
	Contador = 2
	
	Escribir "***************************************"
	Para ciclo = 1 Hasta tamaņoMatriz - 1 Con Paso 1 Hacer
				
		
		Si ( nuevafila - 1 ) = 0 Entonces
			nuevafila = filas
		SiNo
			nuevafila = nuevafila -1
		FinSi
		
		Si (nuevaColumna + 1) > columnas Entonces
			nuevaColumna = 1
		SiNo
			nuevaColumna = nuevaColumna + 1
		FinSi
		
		Si lista[nuevafila,nuevaColumna] == 0 Entonces			
			
			lista[nuevafila,nuevaColumna] = Contador
			Escribir "Valor ingresado [",nuevafila,",",nuevaColumna,"]", " = ",  lista[nuevafila,nuevaColumna]
			filaAnterior    = nuevafila
			ColumnaAnterior = nuevaColumna
			
		SiNo
			
			Escribir "Se encontro un valor ingresado en la posicion [",nuevafila,",",nuevaColumna,"]", " = ", lista[nuevafila,nuevaColumna]			
			Escribir "Valor nuevo para la posicion [",filaAnterior + 1,",",ColumnaAnterior,"]", " = ", Contador
			
			lista[filaAnterior + 1,ColumnaAnterior] = Contador
			nuevafila = filaAnterior + 1
			nuevaColumna = ColumnaAnterior
			
		FinSi
		
		Contador = Contador + 1
		
	Fin Para
	
	Escribir "***************************************"
	
	Para fila = 1 Hasta filas Con Paso 1 Hacer
		
		Para columna = 1 Hasta columnas Con Paso 1 Hacer
						
			Escribir "Valor para la posicion [",fila,",",columna,"]", " = ", lista[fila,columna]
			
		Fin Para
		
		
		
	Fin Para
	
	
	
	
FinProceso

