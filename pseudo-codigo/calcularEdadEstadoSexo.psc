Proceso  calcularEdadEstadoSexo
	
	totalPersonas   = 2
	mayoresEdad     = 0	
	contador        = 0
	personaSoltero  = 0
	hombreAlto      = 0
	sumaEdad        = 0
	sumaEstatura    = 0
	Repetir
		
		Escribir "Ingrese la edad"
		leer edad
		
		sumaEdad = sumaEdad + edad
		SI edad > 18 Entonces
			mayoresEdad = mayoresEdad + 1
		FinSi
		
		Escribir "Seleccion el tipo de sexo (1: Hombre, 2: Mujer)"
		leer sexo
		
		Escribir "Ingrese la estatura"
		leer estatura
		sumaEstatura = sumaEstatura + estatura
		
		SI sexo == 1  & estatura > 1.70 Entonces
			hombreAlto = hombreAlto + 1
		FinSi		
		
		
		Escribir "Seleccion el estado civil (1: Soltero, 2: Casado)"
		leer estadoCivil
		
		SI estadoCivil == 1 Entonces
			personaSoltero = personaSoltero + 1
		FinSi
		
		
		contador = contador + 1		
	Hasta Que contador >= totalPersonas 
	
	Escribir "Personas mayores de edad : ", mayoresEdad
	Escribir "Personas solteras : ", personaSoltero
	Escribir "Hombres con estatura mayor de 1.70 : ", hombreAlto
	Escribir "El promedio de edad : ", sumaEdad / contador
	Escribir "El promedio de estatura : ", sumaEstatura / contador
	Escribir "El porcentaje de esas personas con respecto a total de personas (500) : ", (contador / 500) * 100, "%"
		
	
FinProceso

