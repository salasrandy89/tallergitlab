Proceso  tallerVectores
	
	sumaValoresImpares        = 0
	totalNumerospares         = 0
	sumaValoresPares          = 0
	valorMenorVector          = 0
	contadorNumerosIngresados = 0
	
	Escribir  "Ingrese la cantidad de numeros a digitar"
	Leer tamaņoDigitado
	Dimension lista[tamaņoDigitado]
	
	Repetir
		
		contadorNumerosIngresados = contadorNumerosIngresados + 1 
		Escribir  "Ingrese un numero"
		Leer numeroIngresado
		lista[contadorNumerosIngresados] = numeroIngresado
		valorMenorVector = numeroIngresado
		
	Hasta Que contadorNumerosIngresados = tamaņoDigitado
	
	Para i = 1 Hasta tamaņoDigitado Con Paso 1 Hacer
		
		Si (lista[i] mod 2) == 0 Entonces
			sumaValoresPares = sumaValoresPares + lista[i]
			totalNumerospares = totalNumerospares + 1
		Sino 
			sumaValoresImpares = sumaValoresImpares + lista[i]
		FinSi
		
		Si lista[i] < valorMenorVector  Entonces
			valorMenorVector = lista[i]
	     FinSi
		
	 Fin Para
	 
	 Escribir " el promedio numeros pares: ", sumaValoresPares / totalNumerospares
	 Escribir " la suma valores impares es : ", sumaValoresImpares
	 Escribir " el valor menor del vector es : ", valorMenorVector
	
	
FinProceso

