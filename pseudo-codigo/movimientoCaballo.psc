Proceso  movimientoCaballo
	
	Dimension  tablaAjedrez[8,8]
	Dimension  movimientosBuenos[64,2]
	Dimension  movimientosMalos[30000,2]
	Dimension  movimientosCaballo[8,2]
	valorEncontrado = 0	
	filaInicial = 8
	columnaInicial = 8
	
	caballoFila    = filaInicial
	caballoColumna = columnaInicial
	Contador = 1
	contadorMovimientosMalos = 0
	
	// Primera posicion del caballo con el valor 1
	tablaAjedrez[caballoFila , caballoColumna]  = 1
	movimientosBuenos[Contador , 1] = caballoFila
	movimientosBuenos[Contador , 2] = caballoColumna
	
Repetir
	// Calcular diferentes movimientos del caballo
	movimientosCaballo[1,1] = caballoFila - 2
	movimientosCaballo[1,2] = caballoColumna +1
	
	movimientosCaballo[2,1] = caballoFila - 1
	movimientosCaballo[2,2] = caballoColumna + 2
	
	movimientosCaballo[3,1] = caballoFila + 1
	movimientosCaballo[3,2] = caballoColumna + 2
	
	movimientosCaballo[4,1] = caballoFila + 2
	movimientosCaballo[4,2] = caballoColumna + 1
	
	movimientosCaballo[5,1] = caballoFila + 2
	movimientosCaballo[5,2] = caballoColumna - 1
	
	movimientosCaballo[6,1] = caballoFila + 1
	movimientosCaballo[6,2] = caballoColumna - 2
	
	movimientosCaballo[7,1] = caballoFila - 1
	movimientosCaballo[7,2] = caballoColumna - 2
	
	movimientosCaballo[8,1] = caballoFila - 2
	movimientosCaballo[8,2] = caballoColumna - 1
	
	// validar movimientos validos
	
	Para fila=1 hasta 8 Hacer
		
		SI movimientosCaballo[fila, 1] < 1 | movimientosCaballo[fila, 1] > 8 Entonces
			movimientosCaballo[fila, 1] = 0
			movimientosCaballo[fila, 2] = 0
		FinSi
		
		SI movimientosCaballo[fila, 2] < 1 | movimientosCaballo[fila, 2] > 8 Entonces
			movimientosCaballo[fila, 1] = 0
			movimientosCaballo[fila, 2] = 0
		FinSi
				
	FinPara
	
	// Eliminar movimientos que no funcionaron
	
	Si contadorMovimientosMalos > 0 Entonces	
	
		Para fila = 1 hasta 8 Hacer
			
			Para comparar = 1 Hasta 30000 Con Paso 1 Hacer
				
				Si movimientosCaballo[fila, 1] = movimientosMalos[comparar, 1] & movimientosCaballo[fila, 2] = movimientosMalos[comparar, 2]  Entonces
					movimientosCaballo[fila, 1] = 0
					movimientosCaballo[fila, 2] = 0
				FinSi
				
			Fin Para
			
		FinPara
		
    FinSi
	
	// Eliminar movimientos que ya exixten 
	Para fila = 1 hasta 8 Hacer
		
		Para comparar = 1 Hasta 64 Con Paso 1 Hacer
			
		    Si movimientosCaballo[fila, 1] = movimientosBuenos[comparar, 1] & movimientosCaballo[fila, 2] = movimientosBuenos[comparar, 2]  Entonces
				movimientosCaballo[fila, 1] = 0
				movimientosCaballo[fila, 2] = 0
			FinSi
			
		Fin Para
		
	FinPara
	
		
	// ver movimientos
//	Para fila = 1 hasta 8 Hacer
//		Escribir "Diferentes movimientos: movimientos ",fila ,": ", movimientosCaballo[fila, 1],",",movimientosCaballo[fila, 2] 
//	FinPara
	
	//Escribir " ************************************ "
	
	Para fila = 1 hasta 8 Hacer
		
		Si movimientosCaballo[fila, 1] > 0 & valorEncontrado = 0 Entonces
			
			Contador = Contador + 1
			caballoFila     = movimientosCaballo[fila, 1]
			caballoColumna  = movimientosCaballo[fila, 2]
			movimientosBuenos[Contador , 1] = caballoFila
			movimientosBuenos[Contador , 2] = caballoColumna
			valorEncontrado = 1
			tablaAjedrez[caballoFila , caballoColumna]  = Contador
			
			Escribir "Movimientos del Caballo: movimientos :", movimientosBuenos[Contador, 1],",",movimientosBuenos[Contador, 2], " = ", Contador
			
		FinSi
		
	FinPara
	
	Si valorEncontrado = 1 Entonces		
		valorEncontrado = 0
		
	SiNo
		
		contadorMovimientosMalos = contadorMovimientosMalos + 1
		
		filaEliminar    =  movimientosBuenos[Contador , 1]
		columnaEliminar = movimientosBuenos[Contador , 2] 
		
		
		Si filaEliminar = filaInicial &  columnaEliminar = columnaInicial Entonces
			caballoFila    = filaInicial
			caballoColumna = columnaInicial
			Contador = 1
			movimientosBuenos[1 , 1] = filaInicial
			movimientosBuenos[1 , 1] = columnaInicial			
			
			ultimaFilaEliminada      =  movimientosMalos[contadorMovimientosMalos - 1, 1]
			eliminarColumnaEliminada = movimientosMalos[contadorMovimientosMalos - 1, 2]
			
			Para comparar = 1 Hasta contadorMovimientosMalos Con Paso 1 Hacer
				
				movimientosMalos[comparar, 1] = 0
				movimientosMalos[comparar, 2] = 0
				
			Fin Para
			
			movimientosMalos[1 , 1] = ultimaFilaEliminada
			movimientosMalos[1 , 2] = eliminarColumnaEliminada
			
			contadorMovimientosMalos = 1
			valorEncontrado = 0
			
		SiNo
			
			movimientosMalos[contadorMovimientosMalos , 1] = filaEliminar
			movimientosMalos[contadorMovimientosMalos , 2] = columnaEliminar
			
			movimientosBuenos[Contador , 1] = 0
			movimientosBuenos[Contador , 2] = 0
			
			Contador = Contador - 1	
			caballoFila     = movimientosBuenos[Contador, 1]
			caballoColumna  = movimientosBuenos[Contador, 2]
			valorEncontrado = 0
		 	Escribir "Movimientos malos del Caballo: movimientos :", movimientosMalos[contadorMovimientosMalos, 1],",",movimientosMalos[contadorMovimientosMalos, 2]
		
		FinSi
			
		
		
	FinSi 
	
Hasta Que Contador = 64

	
	
	
FinProceso

